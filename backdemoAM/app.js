const express = require('express');
const app = express();
const port = 3000;
const cors = require('cors');
const prometheus = require('prom-client');

const myCounter = new prometheus.Counter({
  name: 'compteur appel',
  help: 'Description of the counter metric',
})

let requestCount=0
app.use(cors());

app.get('/', (req, res) => {

  myCounter.inc();
  requestCount=myCounter.get()
  res.send({ "message": "Hello World n° ${requestCount}"});
});

app.get('/health', (req, res) => {

  setTimeout(() => {
    if (requestCount % 5 === 0) {
      res.status(500).send(`Simulated error - Request Count: ${requestCount}`);
    } else {
      res.send(`Health check passed - Request Count: ${requestCount}`);
    }
  }, 15000); // Deliberate delay of 15 seconds
});

// Exposition des métriques via une route /metrics
app.get('/metrics', (req, res) => {
  res.set('Content-Type', prometheus.register.contentType);
  res.end(prometheus.register.metrics());
});



app.listen(port, () => {
  console.log(`Serveur en cours d'exécution sur le port ${port}`);
});



//pt d'entree reponse "isalive" avec 1 erreur toutes les 100S


// rolling update  /upgrade k8S  //#red pill blue pill
//interruption de service pdt les montee de version -appli web non intialisee