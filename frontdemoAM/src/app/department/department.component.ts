import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface ApiResponse {
  message: string;
};

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.css']
})
export class DepartmentComponent  implements OnInit{
  constructor(private http: HttpClient) { }
dataduback: string ="";

  ngOnInit() {
    this.fetchUserData();
  }



  fetchUserData() {
  const userId = 123;
  const username = 'john.doe';
  
  this.http.get<ApiResponse>(`http://localhost:3000`).subscribe((response) => {
    console.log('Réponse du serveur :', response);
    this.dataduback = response['message'];
  },  (error) => {
    console.error('Une erreur s\'est produite :', error);
  }
  );

}


}
